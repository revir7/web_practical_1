﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_s10195019.Models
{
    public class Branch
    {
        [Display(Name = "ID")]
        public int BranchNo { get; set; }

        public string Address { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"[689]\d{7}|\+65[689]\d{7}$", ErrorMessage = "Invalid Singapore Phone Number")]
        public string Telephone { get; set; }
        
        
    }
}
