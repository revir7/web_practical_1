﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace web_s10195019.Models
{
    public class Staff
    {
        [Display(Name = "ID")]
        public int StaffId { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(50, ErrorMessage = "name should be lesser than 50 characters")]
        public string Name { get; set; }

        public char Gender { get; set; }

        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }

        public string Nationality { get; set; }

        [Display(Name = "Email Address")]
        [EmailAddress]//validation for email address format
        // Custom Validation Attribute for checking email address exists 
        [ValidateEmailExists] 
        public string Email { get; set; }

        [Range(1.00, 10000.00, ErrorMessage = "salary must be between 1.00 to 10000.00")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        [Display(Name = "Monthly Salary (SGD)")]
        public decimal Salary { get; set; }

        [Display(Name = "Full-Time Staff")]
        public bool IsFullTime { get; set; }
        
        [Display(Name = "Branch")]
        public int? BranchNo { get; set; }
    }
}
